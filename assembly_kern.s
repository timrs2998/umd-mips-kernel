#
# File: assembly_kern.s
# Author: Tim Schoenheider, Jay Stoneburner
#
# Contains assembly code that the kernel depends on.
# testSyscall is not regarded as part of the kernel.
#

    .data

	.set noat
    .global db_puts
    .global db_gets
    .global printf

    .global Getv0
    .global Getv1

    .text
/**********************************************
# Procedure: Getv0
# input: none
# output: $v0 has the previous $v0
# Effect: Does nothing, retains current $v0 and exits
#         Will not change other registers (ie: $v1)
**********************************************/
Getv0:
    jr $ra


/**********************************************
# Procedure: GetV1
# input: none
# output: $v0 has $v1
# Effect: Copies $v1 into $v0, changes no other values
**********************************************/
Getv1:
    move $v0, $v1
    jr $ra

