# UMD MIPS Kernel #

An interrupt-driven kernel for MIPS PICS2 devices written for CS 2521 under Prof. Chris Prince. The kernel handles interrupts and performing syscalls such as printing to console or turning lights on/off for a menu-driven application.

### Dependencies ###

Before you get started, you should have a MIPS PIC32 device or be prepared to emulate it. Download and install the following:

* [MPLab XC32 Compiler](http://www.microchip.com/pagehandler/en_us/devtools/mplabxc/)
* [MPLab X IDE](http://www.microchip.com/pagehandler/en-us/family/mplabx/)

### Compiling ###

Once the IDE and compiler are installed, start the IDE. You should be able to open the umd-mips-kernel/ folder in the IDE. Once opened, you can compile the code and deploy it to a MIPS PIC32 device or setup a simulation.

### Licensing ###

All code is licensed under the GNU GENERAL PUBLIC LICENSE Version 3.