#
# File: assembly_app.s
# Author: Tim Schoenheider, Jay Stoneburner
#
# Contains assembly code needed for the application.
# testSyscall is regarded as part of the application.
#

    .data

	.set noat

    .global Setv0v1

    .text


/**********************************************
# Procedure: Setv0v1
# input: $a0 has the value to store in $v0
#        $a1 has the value to store in $v1
# output: $a0 and $a1 are in $v0 and $v1 respectively
**********************************************/
Setv0v1:
    move $v0, $a0
    move $v1, $a1
    jr   $ra
