/* 
 * File:   kernel.c
 * Author: Tim Schoenheider, Jay Stoneburner
 *
 * Created on 27 de noviembre de 2012, 23:56
 */

#include <stdio.h>
#include <stdlib.h>
#include <plib.h>
#include <p32xxxx.h>

#include "kernel.h"
#include "application.h"

/*
 * 
 */
void main() {
    initializeLEDs();
    initializeButtons();

    run_app();
    
    while(1){
        // infinite loop
    }
}

void testSyscall(int p1, int p2, int p3, int p4, int p5){
    // This is NOT regarded as part of the kernel.
    // 
    // Thus option 9 in the menu simply calls this
    // function.
    // 
    
    char string[7];
    string[0] = 48 + p1;
    string[1] = 48 + p2;
    string[2] = 48 + p3;
    string[3] = 48 + p4;
    string[4] = 48 + p5;
    string[5] = '\n';
    string[6] = '\0';

    Setv0v1(KERNEL_PRINT, (int) string);
    asm("syscall");
}


void kernelPrint(int kernelOperationCode, char *outputBuffer){
    if(kernelOperationCode == KERNEL_PRINT){
        if(DEBUG){
            printf("KERNEL: KERNEL_PRINT requested\n");
            printf("KERNEL: Pointer sent by call to print: 0x%x\n", &outputBuffer);
        }
        printf("%s", outputBuffer);
    }
}


void kernelGetString(int kernelOperationCode, char *inputBuffer, int length){
    if(kernelOperationCode == KERNEL_GETSTRING){
        if(DEBUG){
            printf("KERNEL: KERNEL_GETSTRING requested\n");
            printf("KERNEL: inputBuffer = 0x%x   and   length = %d\n",
                    inputBuffer,
                    length
            );
        }

        // Initialize array to null characters
        int i = 0;
        for(i = 0; i < length + 1; i++){
            inputBuffer[i] = '\0';
        }
        
        // Get user input
        db_gets(inputBuffer, length);
    }
}


void kernelLEDAction(int kernelOperationCode, int LEDNumber, int LEDState){
    if(kernelOperationCode == KERNEL_LEDACTION){
        if(DEBUG){
            printf("KERNEL: KERNEL_LEDACTION requested\n");
            printf("KERNEL: LED Number: %d\n", LEDNumber);
            printf("KERNEL: LED State: %d\n", LEDState);
        }
        
        if (LEDNumber == LED_NUMBER_1) {
            if (LEDState == LED_ON)
                mPORTDSetBits(BIT_0);
            else
                mPORTDClearBits(BIT_0);
        }
        else if (LEDNumber == LED_NUMBER_2) {
            if (LEDState == LED_ON)
                mPORTDSetBits(BIT_1);
            else
                mPORTDClearBits(BIT_1);
        }
        else if (LEDNumber == LED_NUMBER_3) {
            if (LEDState == LED_ON)
                mPORTDSetBits(BIT_2);
            else
                mPORTDClearBits(BIT_2);
        }
    }
}

void kernelRegisterButtonPressHandler(int kernelOperationCode, int ButtonNumber, ButtonPressHandlerType handler){
    if(kernelOperationCode == KERNEL_REGISTERBUTTONHANDLER){
        if(DEBUG){
            printf("KERNEL: KERNEL_REGISTERBUTTONHANDLER requested\n");
            printf("KERNEL: Button Number: %d\n", ButtonNumber);
            printf("KERNEL: Button Handler: 0x%x\n", handler);
        }
        
        if(ButtonNumber == BUTTON_NUMBER_1){
            buttonOneHandler = handler;
        }
        else if(ButtonNumber == BUTTON_NUMBER_2){
            buttonTwoHandler = handler;
        }
        else if(ButtonNumber == BUTTON_NUMBER_3){
            buttonThreeHandler = handler;
        }
    }
}


void initializeButtons(void) {
    // Set function pointer to 0
    buttonOneHandler = 0;
    buttonTwoHandler = 0;
    buttonThreeHandler = 0;

    // Initialize previous PORTD to 0
    prev_portd = 0x00000000;

    // STEP 1. configure the wait states and peripheral bus clock
    SYSTEMConfigWaitStatesAndPB(72000000L);

    // STEP 2. configure the port registers
    PORTSetPinsDigitalOut(IOPORT_A, BIT_2 | BIT_3);
    PORTSetPinsDigitalIn(IOPORT_D, BIT_6);

    // STEP 3. initialize the port pin states = outputs low
    mPORTAClearBits(BIT_2 | BIT_3);

    // STEP 4. setup the change notice options
    mCNOpen(CONFIG, PINS, PULLUPS);

    // STEP 5. read port(s) to clear mismatch on change notice pins
    // value = mPORTDRead();
    int tmp = PORTD;

    // STEP 6. clear change notice interrupt flag
    ConfigIntCN(INTERRUPT);

    // STEP 7. enable multi-vector interrup
    INTEnableSystemMultiVectoredInt();
}

void initializeLEDs(void){
    /* setup LEDs */
    mPORTDClearBits(BIT_0);         /* Turn off LED1 on startup */
    mPORTDSetPinsDigitalOut(BIT_0); /* Make RD0 (LED1) as output */

    mPORTDClearBits(BIT_1);         /* Turn off LED2 on startup */
    mPORTDSetPinsDigitalOut(BIT_1); /* Make RD0 (LED2) as output */

    mPORTDClearBits(BIT_2);         /* Turn off LED3 on startup */
    mPORTDSetPinsDigitalOut(BIT_2); /* Make RD0 (LED3) as output */
}


void _general_exception_handler(void){
    // Save $v0 and $v1 for syscall if needed later
    int v0 = Getv0();
    int v1 = Getv1();

    asm volatile("mfc0 %0,$13" : "=r" (_excep_code));
    asm volatile("mfc0 %0,$14" : "=r" (_excep_addr));

    _excep_code = (_excep_code & 0x0000007C) >> 2;

    // Get cause and status
    unsigned int cause = _CP0_GET_CAUSE();
    unsigned int status = _CP0_GET_STATUS();
    
    
    if(DEBUG){
        printf("KERNEL: cause= %u, status= %u\n", cause, status);
        printf("KERNEL: Exception code= %d\n", _excep_code);
    }


    switch(_excep_code){
        // Hardware interrupt
        case EXCEP_IRQ:
            printf("KERNEL: Hardware interrupt!\n");
            break;

        // Alignment
        case EXCEP_AdEL:
        case EXCEP_AdES:
        case EXCEP_IBE:
        case EXCEP_DBE:
            printf("KERNEL: Alignment exception!\n");
            break;

        // Syscall
        case EXCEP_Sys:
            if(DEBUG)
                printf("KERNEL: Syscall exception!\n");

            // Call to proper function
            if(v0 == KERNEL_PRINT){
                kernelPrint(KERNEL_PRINT, (char *) v1);
            }
            else if(v0 == KERNEL_GETSTRING){
                // assume v1 is a 2 element int array
                int buffer = ((int * )v1)[0];
                int length = ((int * )v1)[1];

                if(DEBUG)
                    printf("KERNEL: Syscall with buffer: %x   length: %d\n",
                        buffer, 
                        length
                    );

                kernelGetString(KERNEL_GETSTRING, (char * ) buffer, length);
            }
            else if(v0 == KERNEL_LEDACTION){

                // assume that v1 is an address of a size 2 integer array
                int number = ((int *)v1)[0];
                int state = ((int *)v1)[1];
                
                if(DEBUG)
                    printf("KERNEL: Syscall with led: num = %d    state = %d\n", 
                        number, 
                        state
                    );
                kernelLEDAction(KERNEL_LEDACTION, number, state);
            }
            else if(v0 == KERNEL_REGISTERBUTTONHANDLER){

                // assume v1 is a 2 element integer array
                int button = ((int *)v1)[0];
                int handler = ((int *)v1)[1];
                
                if(DEBUG)
                    printf("KERNEL: Syscall to register handler at 0x%x to button %d\n",
                        handler, 
                        button
                    );
                
                kernelRegisterButtonPressHandler(KERNEL_REGISTERBUTTONHANDLER, 
                    button,
                    (ButtonPressHandlerType) handler
                );
            }
            break;

        // Breakpoint
        case EXCEP_Bp:
            printf("KERNEL: Breakpoint exception!\n");
            break;

        // Other exception
        default:
             printf("KERNEL: Unknown exception!\n");
            break;
    } // end switch


    if(DEBUG)
        printf("\n");


    // increment EPC by 4 bytes
    _CP0_SET_EPC(_CP0_GET_EPC() + 4);
}

void __ISR(_CHANGE_NOTICE_VECTOR, ipl2) ChangeNotice_Handler(void){
    unsigned int value;
    value = PORTD;

    IFS1CLR = 0x0001;
    // On first press, set prev_portd and quit
    if(prev_portd == 0x00000000){
        prev_portd = value;
    }
    else if( (prev_portd ^ value) == 0x40){
        if(DEBUG)
            printf("KERNEL: Button 1 was pressed\n");

        if(*buttonOneHandler != 0)
            buttonOneHandler();
    }
    else if( (prev_portd ^ value) == 128){
        if(DEBUG)
            printf("KERNEL: Button 2 was pressed\n");

        if(*buttonTwoHandler != 0)
            buttonTwoHandler();

    }
    else if( (prev_portd ^ value) == 8192){
        if(DEBUG)
            printf("KERNEL: Button 3 was pressed\n");
        
        if(*buttonThreeHandler != 0)
            buttonThreeHandler();

    }
    prev_portd = value;
}
