/* 
 * File:   application.c
 * Author: Tim Schoenheider, Jay Stoneburner
 *
 * Created on 27 de noviembre de 2012, 23:56
 */

#include <stdio.h>
#include "kernel.h"
#include "application.h"

/*
 * Note: set DEBUG in kernel.h to hide/show kernel messages
 * 
 * kernel.h is needed for the constants as well as testSyscall
 * which is called directly.
 * 
 * testSyscall is the only function called in the kernel, 
 * this appears to be a lab requirement. There is no way to 
 * reach it through syscall. Thus it is not regarded as part
 * of the kernel.
 *
 *
 * Testing button presses without debugging:
 * NOTE: To test button presses after program exits and without
 * debugging, choose option c to attach handlers. Then quit (0 
 * or q). Pressing buttons then toggles the lights. 
 * 
 */
 

void run_app(void){
    // Assume all lights are off
    led_one_state = 0;
    led_two_state = 0;
    led_three_state = 0;

    int loop = 1;

    int length = 1;
    char * input[length + 1];

    while(loop){
        printMenu();

        int buffer_and_length[] = {
            (int) input,
            length
        };
        do_syscall_exception(KERNEL_GETSTRING, (int)buffer_and_length);

        do_syscall_exception(KERNEL_PRINT, (int)"User typed: ");
        do_syscall_exception(KERNEL_PRINT, (int)input);
        do_syscall_exception(KERNEL_PRINT, (int)"\n");

        loop = handleInput((char *) input);
    } // end while

}

void do_syscall_exception(int function, int ptr){
    Setv0v1((int)function, (int)ptr);
    asm("syscall");
}

void button_one_press_handler(void){
    button_flag = 1;
}
void button_two_press_handler(void){
    button_flag = 2;
}
void button_three_press_handler(void){
    button_flag = 3;
}

void printMenu(void){
        do_syscall_exception(KERNEL_PRINT,(int)"Menu:\n");
        do_syscall_exception(KERNEL_PRINT,(int) "   0) Quit / Exit\n");          
        do_syscall_exception(KERNEL_PRINT,(int)"   1) Turn LED 1 on\n");          
        do_syscall_exception(KERNEL_PRINT,(int)"   2) Turn LED 1 off\n");         
        do_syscall_exception(KERNEL_PRINT,(int)"   3) Turn LED 2 on\n");          
        do_syscall_exception(KERNEL_PRINT,(int)"   4) Turn LED 2 off\n");         
        do_syscall_exception(KERNEL_PRINT,(int)"   5) Turn LED 3 on\n");          
        do_syscall_exception(KERNEL_PRINT,(int)"   6) Turn LED 3 off\n");         
        do_syscall_exception(KERNEL_PRINT,(int)"   7) Wait for a button press\n");
        do_syscall_exception(KERNEL_PRINT,(int)"   8) Do break exception\n");     
        do_syscall_exception(KERNEL_PRINT,(int)"   9) Do alignment exception\n"); 
        do_syscall_exception(KERNEL_PRINT,(int)"   a) Do syscall exception\n");    
        do_syscall_exception(KERNEL_PRINT,(int)"   b) Flash lights waiting for button press\n"); 
        do_syscall_exception(KERNEL_PRINT,(int)"   c) Toggle lights on button presses\n");
        do_syscall_exception(KERNEL_PRINT,(int)"Enter value 0 through c (hex): \n"); 
}

int handleInput(char * char_array){
        char input = char_array[0];
        if(input == '1'){
            int param[] = { LED_NUMBER_1, LED_ON };
            do_syscall_exception(KERNEL_LEDACTION, (int)param );
            led_one_state = 1;
        }
        else if(input == '2'){
            int param[] = { LED_NUMBER_1, LED_OFF };
            do_syscall_exception(KERNEL_LEDACTION, (int) param );
            led_one_state = 0;
        }
        else if(input == '3'){
            int param[] = { LED_NUMBER_2, LED_ON };
            do_syscall_exception(KERNEL_LEDACTION, (int) param );
            led_two_state = 1;
        }
        else if(input == '4'){
            int param[] = { LED_NUMBER_2, LED_OFF };
            do_syscall_exception(KERNEL_LEDACTION, (int) param );
            led_two_state = 0;
        }
        else if(input == '5'){
            int param[] = { LED_NUMBER_3, LED_ON };
            do_syscall_exception(KERNEL_LEDACTION, (int) param );
            led_three_state = 1;
        }
        else if(input == '6'){
            int param[] = { LED_NUMBER_3, LED_OFF };
            do_syscall_exception(KERNEL_LEDACTION, (int) param );
            led_three_state = 0;
        }
        else if(input == '7'){
            do_syscall_exception(KERNEL_PRINT, 
                (int) "Waiting for button press.\n");

            // Attach handlers
            int param1[] = { BUTTON_NUMBER_1,
                (int) button_one_press_handler };
            do_syscall_exception(KERNEL_REGISTERBUTTONHANDLER, (int) param1 );

            int param2[] = { BUTTON_NUMBER_2,
                (int) button_two_press_handler };
            do_syscall_exception(KERNEL_REGISTERBUTTONHANDLER, (int) param2 );

            int param3[] = { BUTTON_NUMBER_3,
                (int) button_three_press_handler };
            do_syscall_exception(KERNEL_REGISTERBUTTONHANDLER, (int) param3 );

            // Wait for flag to change
            button_flag = 0;
            while(!button_flag){
                // do nothing
            }
            
            // Output which button was pressed
            if(button_flag == 1)
                do_syscall_exception(KERNEL_PRINT,
                    (int)"Button 1 pressed!\n");
            else if(button_flag == 2)
                do_syscall_exception(KERNEL_PRINT,
                    (int)"Button 2 pressed!\n");
            else
                do_syscall_exception(KERNEL_PRINT,
                    (int)"Button 3 pressed!\n");
        }
        else if(input == '8'){
            asm("break");
            asm("nop");
        }
        else if(input == '9'){
            asm("lw $t1, 1($zero)");
            asm("nop");
        }
        else if(input == 'a'){
            // Note that this is not regarded as
            // part of the kernel. Can't be reached 
            // through syscall.
            testSyscall(1, 2, 3, 4, 5);
        }
        else if(input == 'b'){
            lightShow();
        }
        else if(input == 'c'){
            toggleLightsOnPress();
        }
        else if(input == '0' || input == 'q'){
            do_syscall_exception(KERNEL_PRINT, 
                (int)"Quitting.\n");
            return 0;
        }
        else{
            do_syscall_exception(KERNEL_PRINT, 
                (int)"Invalid input.\n");
        }
        return 1;
}


void lightShow(void){
    // Initialize the flag
    button_flag = 0;


    // Register the button handlers
    int param1[] = { BUTTON_NUMBER_1,
        (int) button_one_press_handler };
    do_syscall_exception(KERNEL_REGISTERBUTTONHANDLER, (int) param1 );

    int param2[] = { BUTTON_NUMBER_2,
        (int) button_two_press_handler };
    do_syscall_exception(KERNEL_REGISTERBUTTONHANDLER, (int) param2 );

    int param3[] = { BUTTON_NUMBER_3,
        (int) button_three_press_handler };
    do_syscall_exception(KERNEL_REGISTERBUTTONHANDLER, (int) param3 );


    do_syscall_exception(KERNEL_PRINT, 
        (int)"Press button to stop.\n");


    while(!button_flag){
        int i = 0;

        toggleLEDOne();

        // Wait a while
        i = 0;
        while(i < (100000 / 4)){
            i++;
        }
        toggleLEDTwo();

        // Wait a while
        i = 0;
        while(i < (100000 / 4)){
            i++;
        }

        toggleLEDThree();

        // Wait a while
        i = 0;
        while(i < (100000 / 4)){
            i++;
        }

    }
    do_syscall_exception(KERNEL_PRINT, 
        (int)"Button pressed.\n");
}



void toggleLEDOne(void){
    if(led_one_state){
        int param[] = { LED_NUMBER_1, LED_OFF };
        do_syscall_exception(KERNEL_LEDACTION, (int)param );
        led_one_state = 0;
    }
    else{
        int param[] = { LED_NUMBER_1, LED_ON };
        do_syscall_exception(KERNEL_LEDACTION, (int)param );
        led_one_state = 1;
    }
}

void toggleLEDTwo(void){
    if(led_two_state){
        int param[] = { LED_NUMBER_2, LED_OFF };
        do_syscall_exception(KERNEL_LEDACTION, (int)param );
        led_two_state = 0;
    }
    else{
        int param[] = { LED_NUMBER_2, LED_ON };
        do_syscall_exception(KERNEL_LEDACTION, (int)param );
        led_two_state = 1;
    }
}

void toggleLEDThree(void){
    if(led_three_state){
        int param[] = { LED_NUMBER_3, LED_OFF };
        do_syscall_exception(KERNEL_LEDACTION, (int)param );
        led_three_state = 0;
    }
    else{
        int param[] = { LED_NUMBER_3, LED_ON };
        do_syscall_exception(KERNEL_LEDACTION, (int)param );
        led_three_state = 1;
    }
}

void toggleLightsOnPress(void){
    // Register the button handlers
    int param1[] = { BUTTON_NUMBER_1,
        (int) toggleLEDOne };
    do_syscall_exception(KERNEL_REGISTERBUTTONHANDLER, (int) param1 );

    int param2[] = { BUTTON_NUMBER_2,
        (int) toggleLEDTwo };
    do_syscall_exception(KERNEL_REGISTERBUTTONHANDLER, (int) param2 );

    int param3[] = { BUTTON_NUMBER_3,
        (int) toggleLEDThree };
    do_syscall_exception(KERNEL_REGISTERBUTTONHANDLER, (int) param3 );

    // Print a message
    do_syscall_exception(KERNEL_PRINT, 
        (int)"Warning: while dbgets or scanf expects input,\n");
    do_syscall_exception(KERNEL_PRINT, 
        (int)"button press exceptions will fail.\n");

}