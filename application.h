/* 
 * File:   application.h
 * Author: Tim Schoenheider, Jay Stoneburner
 *
 * Created on 28 de noviembre de 2012, 1:04
 */

#ifndef APPLICATION_H
#define	APPLICATION_H

#ifdef	__cplusplus
extern "C" {
#endif

/*
	Main entry point of the application. Presents a menu
	allowing for user input, performs action given by user.
*/
void run_app(void);

/*
	Utility method used to print a menu.
*/
void printMenu(void);

/*
	Utility method used to handle user input.
	input: a character array
	output: 0 if user entered 'q' or '0'
*/
int handleInput(char * input);


// initially 0, becomes 1, 2, 3 on button press
int button_flag;

/*
	Button press handlers, used to set button_flag 
	to 1, 2, or 3 respectively when a button if pressed. 
*/
void button_one_press_handler(void);
void button_two_press_handler(void);
void button_three_press_handler(void);


/*
	Performs a syscall exception with given parameters.
	input: function: the kernel opcode, placed in v0
		   ptr: whatever the operation will be expecting,
		   		always typecalse to int
*/
void do_syscall_exception(int function, int ptr);


/*
	Integers representing the states of the lights.
	0 for off, 1 for on.
*/
int led_one_state;
int led_two_state;
int led_three_state;

/*
	Cycles through the lights, turning them off and on
	to create a pretty visual effect. 

	User must press button to set button_flag and stop
	execution of lightShow()
*/
void lightShow(void);


/*
	Functions that toggle the state of an LED. Turns
	LED on when its off, turns LED off when its on.
*/
void toggleLEDOne(void);
void toggleLEDTwo(void);
void toggleLEDThree(void);


/*
	Attaches toggleLEDOne(), toggleLEDTwo(), and 
	toggleLEDThree() as button handlers to each of 
	the 3 buttons. 
*/
void toggleLightsOnPress(void);


// imported from assembly_app.s:
void Setv0v1(int v0, int v1);


#ifdef	__cplusplus
}
#endif

#endif	/* APPLICATION_H */

