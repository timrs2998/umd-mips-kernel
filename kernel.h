/* 
 * File:   kernel.h
 * Author: Tim Schoenheider, Jay Stoneburner
 *
 * 
 * Created on 27 de noviembre de 2012, 23:56
 */

#ifndef KERNEL_H
#define	KERNEL_H

#ifdef	__cplusplus
extern "C" {
#endif

// Global flag to enable1 kernel printf messages
#define DEBUG 0

// System call operation codes
#define KERNEL_PRINT			80
#define KERNEL_GETSTRING		81
#define KERNEL_LEDACTION		82
#define KERNEL_REGISTERBUTTONHANDLER	83

// LED constants
#define LED_NUMBER_1 1
#define LED_NUMBER_2 2
#define LED_NUMBER_3 3
#define LED_ON 1
#define LED_OFF 0

// Button constants
#define NUMBER_OF_BUTTONS 3
#define BUTTON_NUMBER_1 0
#define BUTTON_NUMBER_2 1
#define BUTTON_NUMBER_3 2

// Button definitions
#define CONFIG          (CN_ON | CN_IDLE_CON)
#define PINS            (CN15_ENABLE | CN16_ENABLE | CN19_ENABLE)
#define PULLUPS         (CN_PULLUP_DISABLE_ALL)
#define INTERRUPT       (CHANGE_INT_ON | CHANGE_INT_PRI_2)

/*
	Prints out 1, 2, 3, 4, 5 as a demo to show that correct parameter
	values can be passed in to the kernel
*/
void testSyscall(int p1, int p2, int p3, int p4, int p5);


/* kernel prints the NULL terminated string contained in the outputBuffer.
	(Stops printing at the NULL).
*/
void kernelPrint(int kernelOperationCode, char *outputBuffer);


/* kernel reads a string from the user and place the characters read into
	the inputBuffer. Will read up to length characters. Does not place
	a newline in the inputBuffer, but does write a terminating NULL into
	that buffer. And any chars after the terminating NULL up to length+1
	will be written with NULLs.
	Caller must make sure inputBuffer has length+1 bytes
	allocated for it (length for characters typed, and one more for the NULL).
*/
void kernelGetString(int kernelOperationCode, char *inputBuffer, int length);

/*
	Changes the state of the light corresponding to LEDNumber to the state
	(on or off) given in LEDState.
*/
void kernelLEDAction(int kernelOperationCode, int LEDNumber, int LEDState);


/*
	Register a handler function for the button presses on the PIC32 board.
	The third argument is a function pointer. For syntax for these, see:
		http://c-faq.com/~scs/cclass/int/sx10c.html
	See below for example of usage of this system call.
*/
typedef void (*ButtonPressHandlerType)(void);


/*
	Pointers to functions that are executed when a button is pressed. 
*/
ButtonPressHandlerType buttonOneHandler;
ButtonPressHandlerType buttonTwoHandler;
ButtonPressHandlerType buttonThreeHandler;


/*
 * Registers a handler that is executed when a button is pressed. IE:
 * RegisterHandler(opCode, 1, function(){ LED's on } );
 *
 * just set buttonOneHandler or whatever to the function,
 * run buttonOneHandler in the the interrupt handler
 */
void kernelRegisterButtonPressHandler(int kernelOperationCode, int ButtonNumber,
	ButtonPressHandlerType);


/*
	Initialized the LEDs to their off state so that their state can be 
	modified later
*/
void initializeLEDs(void);


/*
	Enables interrupt handling for the 3 hardware buttons.
*/
void initializeButtons(void);

// Variable for previous PORTD value (used for buttons)
int prev_portd;


/*
	Function that is executed for any given software interrupt.
*/
void _general_exception_handler(void);


/*
	Handles hardware interrupts such as button presses.
*/
void __ISR(_CHANGE_NOTICE_VECTOR, ipl2);


// imported from assembly_kern.s:
int Getv0();
int Getv1();

// imported from assembly_app.s:
void Setv0v1(int v0, int v1);


// enum containing list of possible exceptions
static enum {
	EXCEP_IRQ = 0,			// interrupt
	EXCEP_AdEL = 4,			// address error exception (load or ifetch)
	EXCEP_AdES,				// address error exception (store)
	EXCEP_IBE,				// bus error (ifetch)
	EXCEP_DBE,				// bus error (load/store)
	EXCEP_Sys,				// syscall
	EXCEP_Bp,				// breakpoint
	EXCEP_RI,				// reserved instruction
	EXCEP_CpU,				// coprocessor unusable
	EXCEP_Overflow,			// arithmetic overflow
	EXCEP_Trap,				// trap (possible divide by zero)
	EXCEP_IS1 = 16,			// implementation specfic 1
	EXCEP_CEU,				// CorExtend Unuseable
	EXCEP_C2E				// coprocessor 2
} _excep_code;

// variables for the exception code and address
static unsigned int _epc_code;
static unsigned int _excep_addr;


#ifdef	__cplusplus
}
#endif

#endif	/* KERNEL_H */

